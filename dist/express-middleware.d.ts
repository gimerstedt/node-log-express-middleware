import { NextFunction, Request, Response } from 'express';
interface ILogOptions {
}
export declare const logger: (opts?: ILogOptions) => (req: Request, res: Response, next: NextFunction) => void;
export {};

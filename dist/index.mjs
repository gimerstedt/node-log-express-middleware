import{log as o}from"@gimerstedt/log";import t from"on-finished";import r from"performance-now";const e=(e={})=>(e,n,m)=>{const i=r();t(n,()=>{const t=((o,t,r=" ")=>{let e=o;for(;e.length<t;)e+=r;return e})(e.method,7),m=n.statusCode||500,s=`${t} ${e.path} (${m}) in ${(r()-i).toFixed(3)}ms`;m>=400?o.error(s):o.info(s)}),m()};export{e as logger};
//# sourceMappingURL=index.mjs.map

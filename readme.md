# log-express-middleware
simple express logging middleware, env-var LOG\_LEVEL controls log level (none, error, warn, info)

## example
``` typescript
import { logger } from '@gimerstedt/log-express-middleware'
import express from 'express'

express()
  .use(logger())
  .get('/y', (_,r) => r.send('y'))
  .get('/n', (_,r) => r.status(500).send('n'))
  .listen(3333)
```

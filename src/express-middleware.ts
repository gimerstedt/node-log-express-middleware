import { log } from '@gimerstedt/log'
import { NextFunction, Request, Response } from 'express'
import onFinished from 'on-finished'
import now from 'performance-now'
import { rightPad } from './util'

interface ILogOptions {}

export const logger = (opts: ILogOptions = {}) => (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  const start = now()
  const fn = () => {
    const method = rightPad(req.method, 7)
    const path = req.path
    const code = res.statusCode || 500
    const tt = (now() - start).toFixed(3)
    const msg = `${method} ${path} (${code}) in ${tt}ms`

    if (code >= 400) log.error(msg)
    else log.info(msg)
  }
  onFinished(res, fn)
  next()
}

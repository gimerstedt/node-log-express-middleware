import { log } from '@gimerstedt/log'
import { NextFunction, Request, Response } from 'express'
import { logger } from './express-middleware'

describe('logger', () => {
  let req: any
  let res: any
  let next: any

  beforeEach(() => {
    req = { method: 'GET', path: 'someplace' }
    res = { statusCode: 200 }
    next = jest.fn()
  })

  it('get 200', async d => {
    const infoSpy = spyOn(log, 'info')

    const l = logger()
    l(req as Request, res as Response, next as NextFunction)

    await new Promise(res => setTimeout(() => res(), 100))

    expect(next).toHaveBeenCalledTimes(1)
    expect(log.info).toHaveBeenCalledTimes(1)

    const infoArg = infoSpy.calls.all()[0].args[0]
    expect(infoArg).toMatch(/^GET     someplace \(200\) in .*\d\d\dms$/)
    d()
  })

  it('delete 500', async d => {
    const errorSpy = spyOn(log, 'error')
    req.method = 'DELETE'
    res.statusCode = 500

    const l = logger()
    l(req as Request, res as Response, next as NextFunction)

    await new Promise(res => setTimeout(() => res(), 100))

    expect(next).toHaveBeenCalledTimes(1)
    expect(log.error).toHaveBeenCalledTimes(1)

    const infoArg = errorSpy.calls.all()[0].args[0]
    expect(infoArg).toMatch(/^DELETE  someplace \(500\) in .*\d\d\dms$/)
    d()
  })
})

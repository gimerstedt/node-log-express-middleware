import { rightPad } from './util'

describe('rightPad', () => {
  it('pads', () => {
    const p = 'a'
    expect(rightPad(p, 2)).toEqual(p + ' ')
    expect(rightPad(p, 3)).toEqual(p + '  ')
    expect(rightPad(p, 9)).toEqual(p + '        ')
    expect(rightPad(p, 1)).toEqual(p)
    expect(rightPad(p, 2, 3)).toEqual(p + '3')

    expect(rightPad('', 2, 3)).toEqual('33')
    expect(rightPad('qwerty', 2, 3)).toEqual('qwerty')
  })
})

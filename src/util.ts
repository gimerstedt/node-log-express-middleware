export const rightPad = (s: string, minLen: number, char: any = ' ') => {
  let ret = s
  while (ret.length < minLen) ret += char
  return ret
}
